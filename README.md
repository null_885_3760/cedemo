# cedemo

#### 介绍
关于5个布局和
用4种布局写出计算器

#### 主要内容在layout中
frame_layout.xml 讲解了frameLayout布局
lin_layout.xml 讲解了LinearLayout布局
rel_layout1.xml 讲解了RelativeLayout布局
table_layout.xml 讲解了TableLayout布局
还有一个布局的介绍在计算器中。

下面是4个计算器内容
jisuanqi_con.xml 用约束布局写的计算器
jisuanqi_lin.xml 用LinearLayout布局写的计算器
jisuanqi_rel.xml 用RelativeLayout布局写的计算器
jisuanqi_table.xml 用TableLayout布局写的计算器

 
 